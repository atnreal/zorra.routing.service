<?php

namespace Zorra\Routing;

use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Database\DatabaseManager;
use Zorra\Routing\Objects\RouteData;

use Zorra\Utils\ApiQuery\PaginateQuery;
use Zorra\Utils\ApiQuery\SortQuery;

class RoutingService implements RoutingServiceInterface
{
    private $service_type;
    private $client_id;
    private $rate_id;

    private $db;
    private $cache;

    public $table_rates = 'rates';
    public $table_prices = 'prices';
    public $table_clients = 'clients';
    public $table_vendors = 'vendors';
    public $table_routes = 'routes';

    private $now;

    public function __construct(DatabaseManager $db, CacheRepository $cache)
    {
        $this->db = $db;
        $this->cache = $cache;

        $this->now = date('Y-m-d H:i:s');
    }

    public function setClient(int $client_id): void
    {
        $this->client_id = $client_id;
    }

    public function setType(int $service_type): void
    {
        $this->service_type = $service_type;
    }

    private function checkRate(): void
    {
        if (empty($this->rate_id)) {
            throw new \Exception("Rate is not set");
        }
    }

    private function checkClient(): void
    {
        if (empty($this->client_id)) {
            throw new \Exception("Client is not set");
        }
    }

    private function checkType(): void
    {
        if (empty($this->service_type)) {
            throw new \Exception("Service type is not set");
        }
    }

    /*
    ########   #######  ##     ## ######## ########  ######
    ##     ## ##     ## ##     ##    ##    ##       ##    ##
    ##     ## ##     ## ##     ##    ##    ##       ##
    ########  ##     ## ##     ##    ##    ######    ######
    ##   ##   ##     ## ##     ##    ##    ##             ##
    ##    ##  ##     ## ##     ##    ##    ##       ##    ##
    ##     ##  #######   #######     ##    ########  ######
     */

    public function getRoutes(array $filters, PaginateQuery $paginate_query, SortQuery $sort_query): object
    {
        $query = $this->db->table($this->table_routes)->where(function ($query) use ($filters) {
            // search name
            if (!empty($filters['search'])) {
                $query->where('name', 'like', '%' . $filters['search'] . '%');
            }
            // client
            if (!empty($filters['client_id'])) {
                $query->whereIn('client_id', $filters['client_id']);
            }
            // vendor
            if (!empty($filters['vendor_id'])) {
                $query->whereIn('vendor_id', $filters['vendor_id']);
            }
            // mcc
            if (!empty($filters['mcc'])) {
                $query->where(['mcc' => $filters['mcc']]);
            }
            // mnc
            if (!empty($filters['mnc'])) {
                $query->where(['mnc' => $filters['mnc']]);
            }
            // code
            if (!empty($filters['code'])) {
                $query->where('code', 'like', $filters['code'] . '%');
            }
        });

        $total = $query->count();
        $page = $meta->processQuery($query);
        $result = $page->get()->toArray();

        return (object) [
            'total' => $total,
            'data' => $result,
        ];

    }

    public function createRoute(array $data): int
    {
        $this->checkClient();
        $this->checkType();

        switch ($this->service_type) {
            case self::SERVICE_SMS:
                $mandatory = ['mcc', 'mnc', 'vendor_id'];
                break;
            case self::SERVICE_VOICE:
                $mandatory = ['code', 'vendor_id'];
                break;
            default:
                $mandatory = ['mcc', 'vendor_id'];
                break;
        }

        foreach ($mandatory as $key) {
            if (!isset($data[$key])) {
                throw new Exception("{$key} is required");
            }
        }
        $data['client_id'] = $this->client_id;
        $id = $this->db->insertGetId($data);
        return $id;
    }

    public function updateRoute(int $route_id, array $data): void
    {
        $immutable = ['id', 'created_at', 'client_id', 'mcc', 'mnc', 'code'];
        $update = array_diff_key($data, array_flip($immutable));
        $update['updated_at'] = $this->now;

        $this->db->table($this->table_routes)->where(['id' => $this->route_id])->update($update);
    }

    public function deleteRoute(int $route_id): void
    {
        $this->db->table($this->table_routes)->where(['id' => $route_id])->delete();
    }

    public function getRouteData($info): RouteData
    {
        $this->checkClient();
        $this->checkType();

        $client_id = $this->client_id;
        $mcc = $info['mcc'];
        $mnc = $info['mnc'];

        $tag = "client.{$this->client_id}";

        switch ($this->service_type) {
            case self::SERVICE_SMS:
                $box = "route.{$client_id}.{$mcc}.{$mnc}";
                break;
            case self::SERVICE_VIBER:
            case self::SERVICE_WHATSAPP:
            case self::SERVICE_HLR:
                $box = "route.{$client_id}.{$mcc}";
                break;
        }

        return $this->cache->tags([$tag])->rememberForever($box, function () use ($client_id, $info) {
            return self::prepareData($info);
        });
    }

    private function prepareData(array $info): RouteData
    {
        $this->checkClient();

        // prepare response
        $route_data = new RouteData(false);

        // check client
        $client = $this->db->table($this->table_clients)->where(['client_id' => $this->client_id])->first();
        if (empty($client)) {
            $route_data->info = 'Client not found';
            return $route_data;
        }

        // check client rate
        $client_rate = $this->db->table($this->table_rates)->find($client->rate_id);
        if (empty($client_rate)) {
            $route_data->info = 'No rate set for the client';
            return $route_data;
        }

        // check client price
        $client_price = $this->getPrice($client->rate_id, $info);
        if (empty($client_price)) {
            $route_data->info = 'Client price not found ';
            return $route_data;
        }

        // get route
        $route = $this->getRoute($info);
        if (empty($route)) {
            $route_data->info = 'Route not found';
            return $route_data;
        }

        // check vendor
        $vendor = $this->db->table($this->table_vendors)->find($route->vendor_id);
        if (empty($vendor)) {
            $route_data->info = 'Vendor not found';
            return $route_data;
        }

        // check vendor rate
        $vendor_rate = $this->db->table($this->table_rates)->find($vendor->rate_id);
        if (empty($vendor_rate)) {
            $route_data->info = 'No rate set for the vendor';
            return $route_data;
        }

        // check vendor price
        $vendor_price = $this->getPrice($vendor->rate_id, $info);
        if (empty($vendor_price)) {
            $route_data->info = 'Vendor price not found ';
            return $route_data;
        }

        $route_data->success = true;
        $route_data->data = [
            'client_rate_currency_id' => $client_rate->currency_id,
            'vendor_rate_currency_id' => $vendor_rate->currency_id,

            'vendor_id' => $vendor->id,
            'client_price' => $client_price->price,
            'vendor_price' => $vendor_price->price,
        ];

        return $route_data;
    }

    private function getPrice($rate_id, $info)
    {
        $now = $this->now;

        if ($this->service_type == self::SERVICE_SMS) {
            // search price by MCC and MNC
            $mcc = $info['mcc'];
            $mnc = $info['mnc'];

            $price = $this->db->table($this->table_prices)->where(['rate_id' => $rate_id])->where(function ($query) use ($mcc, $mnc) {
                $query->where(['mcc' => $mcc, 'mnc' => $mnc])->orWhere(function ($query) use ($mcc, $mnc) {
                    $query->where('mcc', $mcc)->where('mnc', 0);
                })->orWhere(function ($query) use ($mcc, $mnc) {
                    $query->where('mcc', 0)->where('mnc', 0);
                });
            })->where([
                ['start', '<=', $now],
                ['end', '>=', $now],
            ])->orderBy('mcc', 'desc')->orderBy('mnc', 'desc')->first();

        } else {
            // search price by MCC only
            $mcc = $info['mcc'];

            $price = $this->db->table($this->table_prices)->where(['rate_id' => $rate_id])->where(function ($query) use ($mcc) {
                $query->where('mcc', $mcc)->orWhere('mcc', 0);
            })->where([
                ['start', '<=', $now],
                ['end', '>=', $now],
            ])->orderBy('mcc', 'desc')->first();

        }

        return $price ?? false;
    }

    private function getRoute($info)
    {
        if ($this->service_type == self::SERVICE_SMS) {
            // search route by MCC and MNC
            $mcc = (int) $info['mcc'];
            $mnc = (int) $info['mnc'];

            $route = $this->db->table($this->table_routes)->where(['client_id' => $this->client_id])->where(function ($query) use ($mcc, $mnc) {
                $query->where(['mcc' => $mcc, 'mnc' => $mnc])->orWhere(function ($query) use ($mcc, $mnc) {
                    $query->where('mcc', $mcc)->where('mnc', 0);
                })->orWhere(function ($query) use ($mcc, $mnc) {
                    $query->where('mcc', 0)->where('mnc', 0);
                });
            })->orderBy('mcc', 'desc')->orderBy('mnc', 'desc')->first();

        } else {
            // search route by MCC only
            $mcc = $info['mcc'];

            $route = $this->db->table($this->table_routes)->where(['client_id' => $this->client_id])->where(function ($query) use ($mcc) {
                $query->where('mcc', $mcc)->orWhere('mcc', 0);
            })->orderBy('mcc', 'desc')->first();
        }

        return $route ?? false;
    }

    /*
    ########     ###    ######## ########  ######
    ##     ##   ## ##      ##    ##       ##    ##
    ##     ##  ##   ##     ##    ##       ##
    ########  ##     ##    ##    ######    ######
    ##   ##   #########    ##    ##             ##
    ##    ##  ##     ##    ##    ##       ##    ##
    ##     ## ##     ##    ##    ########  ######
     */

    public function setRate(int $rate_id): void
    {
        $this->rate_id = $rate_id;
    }

    public function getRates(array $filters, PaginateQuery $paginate_query, SortQuery $sort_query): object
    {
        $query = $this->db->table($this->table_rates)->where(function ($query) use ($filters) {
            // search name
            if (!empty($filters['search'])) {
                $query->where('name', 'like', '%' . $filters['search'] . '%');
            }
            // currency
            if (!empty($filters['currency_id'])) {
                $query->whereIn('currency_id', $filters['currency_id']);
            }
        });

        $total = $query->count();
        $page = $meta->processQuery($query);
        $result = $page->get()->toArray();

        return (object) [
            'total' => $total,
            'data' => $result,
        ];

    }

    public function createRate($data): int
    {
        $data['created_at'] = $this->now;
        $id = $this->db->table($this->table_rates)->insertGetId($data);
        return $id;
    }

    public function updateRate(array $data): void
    {
        $this->checkRate();

        $immutable = ['id', 'created_at', 'currency_id'];
        $update = array_diff_key($data, array_flip($immutable));
        $update['updated_at'] = $this->now;

        $this->db->table($this->table_rates)->where(['id' => $this->rate_id])->update($update);
    }

    public function wipeRate(): void
    {
        $this->checkRate();
        $this->db->table($this->table_prices)->where(['rate_id' => $this->rate_id])->delete();
    }

    public function deleteRate(): void
    {
        $this->checkRate();
        $this->wipeRate();
        $this->db->table($this->table_rates)->where(['id' => $this->rate_id])->delete();
    }

    public function completeRate(bool $delete_new = true): void
    {
        $this->checkRate();

        if ($delete_new) {
            $sign = '>';
            $table_1 = 'p1';
            $table_2 = 'p2';
        } else {
            $sign = '<';
            $table_1 = 'p2';
            $table_2 = 'p1';
        }

        switch ($this->service_type) {
            case self::SERVICE_VOICE:
                $condition = "p1.code = p2.code";
                break;
            case self::SERVICE_SMS:
                $condition = "p1.mcc = p2.mcc AND p1.mnc = p2.mnc";
                break;
            default:
                $condition = "p1.mcc = p2.mcc";
                break;
        }

        $deleted = $this->db->delete(
            "DELETE p1 FROM prices p1, prices p2 WHERE p1.rate_id = ? AND p2.rate_id = ? AND (p1.mcc = p2.mcc AND p1.mnc = p2.mnc) AND p1.id {$sign} p2.id AND (
                ({$table_1}.start >= {$table_2}.start AND {$table_1}.start <= {$table_2}.end) OR ({$table_1}.end >= {$table_2}.start AND {$table_1}.end <= {$table_2}.end)
            )",
            [$this->rate_id, $this->rate_id]
        );

        $this->db->table($this->table_rates)->where(['id' => $this->rate_id])->update(['state' => self::RATE_STATE_READY]);
    }

    /*
    ########  ########  ####  ######  ########  ######
    ##     ## ##     ##  ##  ##    ## ##       ##    ##
    ##     ## ##     ##  ##  ##       ##       ##
    ########  ########   ##  ##       ######    ######
    ##        ##   ##    ##  ##       ##             ##
    ##        ##    ##   ##  ##    ## ##       ##    ##
    ##        ##     ## ####  ######  ########  ######
     */

    public function getPrices(array $filters, PaginateQuery $paginate_query, SortQuery $sort_query): object
    {
        $this->checkRate();

        $result = $this->db->table($this->table_prices)->where(['rate_id' => $this->rate_id])->where(function ($query) use ($filters) {
            // search name
            if (!empty($filters['search'])) {
                $query->where('name', 'like', '%' . $filters['search'] . '%');
            }
            // mcc
            if (!empty($filters['mcc'])) {
                $query->where(['mcc' => $filters['mcc']]);
            }
            // mnc
            if (!empty($filters['mnc'])) {
                $query->where(['mnc' => $filters['mnc']]);
            }
            // code
            if (!empty($filters['cubrid_error_code()'])) {
                $query->where('code', 'like', $filters['code'] . '%');
            }
        })->sortByQuery($sort_query)->metaPaginate($paginate_query);

        return $result;
    }

    public function addPrices(array $data): int
    {
        $this->checkRate();
        $this->checkType();

        $this->db->table($this->table_rates)->where(['id' => $this->rate_id])->upate(['state' => self::RATE_STATE_PROCESSING]);

        switch ($this->service_type) {
            case self::SERVICE_SMS:
                $mandatory = ['mcc', 'mnc', 'price', 'start', 'end'];
                break;
            case self::SERVICE_VOICE:
                $mandatory = ['code', 'price', 'start', 'end'];
                break;
            default:
                $mandatory = ['mcc', 'price', 'start', 'end'];
                break;
        }

        $batch = [];

        foreach ($data as $row) {
            $skip = false;
            foreach ($mandatory as $key) {
                if (!isset($row[$key])) {
                    $skip = true;
                    break;
                }
            }

            if (!$skip) {
                $row['created_at'] = $this->now;
                $row['rate_id'] = $this->rate_id;
                $batch[] = $row;
            }
        }

        $this->db->insert($batch);

        return count($batch);
    }

    public function updatePrice(int $price_id, array $data): void
    {
        $this->checkRate();

        $immutable = ['id', 'created_at', 'rate_id'];
        $update = array_diff_key($data, array_flip($immutable));
        $update['updated_at'] = $this->now;

        $this->db->table($this->table_prices)->where(['id' => $price_id, 'rate_id' => $this->rate_id])->update($data);
    }

    public function deletePrice(int $price_id): void
    {
        $this->checkRate();

        $this->db->table($this->table_prices)->where(['id' => $price_id, 'rate_id' => $this->rate_id])->delete();
    }

}
