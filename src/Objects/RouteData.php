<?php

namespace Zorra\Routing\Objects;

class RouteData
{
    /**
     * @var bool    $success   indicate success of the request
     * @var array   $data      ['client_rate_currency_id', 'vendor_rate_currency_id', 'vendor_id', 'client_price', 'vendor_price']
     * @var string  $info      contains error desription or reject reason
     */

    public $success;
    public $data;
    public $info;

    public function __construct($success = null, $data = null, $info = null)
    {
        $this->success = $success;
        $this->data = $data;
        $this->info = $info;
    }

}
