<?php

namespace Zorra\Routing;

use Zorra\Routing\Objects\RouteData;
use Zorra\Utils\Objects\Meta;

use Zorra\Utils\ApiQuery\PaginateQuery;
use Zorra\Utils\ApiQuery\SortQuery;

interface RoutingServiceInterface
{
    const SERVICE_SMS = 1;
    const SERVICE_HLR = 2;
    const SERVICE_VIBER = 3;
    const SERVICE_WHATSAPP = 4;
    const SERVICE_VOICE = 5;

    const RATE_STATE_READY = 'ready';
    const RATE_STATE_PROCESSING = 'processing';
    const RATE_STATE_SYNC = 'sync';
    const RATE_STATE_ERROR = 'error';

    // shared

    public function setType(int $type): void;

    public function setClient(int $client_id): void;

    // routing

    public function getRoutes(array $filters, PaginateQuery $paginate_query, SortQuery $sort_query): object;

    public function createRoute(array $data): int;

    public function updateRoute(int $route_id, array $data): void;

    public function deleteRoute(int $route_id): void;

    public function getRouteData($info): RouteData;

    // rates

    public function getRates(array $filters, PaginateQuery $paginate_query, SortQuery $sort_query): object;

    public function setRate(int $rate_id): void;

    public function wipeRate(): void;

    public function createRate(array $data): int;

    public function updateRate(array $data): void;

    public function deleteRate(): void;

    public function completeRate(bool $delete_new = true): void;

    // prices

    public function getPrices(array $filters, PaginateQuery $paginate_query, SortQuery $sort_query): object;

    public function addPrices(array $data): int;

    public function updatePrice(int $price_id, array $data): void;

    public function deletePrice(int $price_id): void;

}
